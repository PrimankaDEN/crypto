package test;


import com.primankaden.crypto.pbkdf2.PBKDF2;
import com.primankaden.crypto.pbkdf2.prf.RandomPRF;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created by PrimankaDEN on 02.04.2017.
 */
public class PBKDF2Test {
    public static void main(String[] args) throws IOException {
        test("pass", "iv", 64, new RandomPRF(128));
        test("___", "testtesttestsss12313", 128, new RandomPRF(128));
        test("resres", "++++++++++++++_________===@@@@@@@@", 96, new RandomPRF(128));
        test("passasdas123asdfasdf1qweqwef", "iv12323r13r132r321r", 256, new RandomPRF(128));
    }

    static void test(String password, String salt, int len, PBKDF2.PRF prf) {
        byte[] password1 = password.getBytes();
        byte[] salt1 = salt.getBytes();
        byte[] result = PBKDF2.pbkdf2(prf, password1, salt1, 512, len);
        if (result.length != len)
            throw new IllegalStateException("Wrong key length " + result.length);
        byte[] result2 = PBKDF2.pbkdf2(prf, password1, salt1, 512, len);
        if (!Arrays.equals(result, result2))
            throw new IllegalStateException("Results are not equal");
        System.out.println(new String(result));
    }
}
