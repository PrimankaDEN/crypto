package test;

import com.primankaden.crypto.base64.Base64;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by PrimankaDEN on 13.02.2017.
 */
public class Base64Test {
    public static void main(String[] args) {
        test("qwerty");
        test("qwerty1");
        test("qwerty12");
        test("");
        test("1");
        test("-789*7-=");
        test("qwerty1234");

        Random r = new Random();
        byte[] data = new byte[64];
        r.nextBytes(data);
        test(data);
        data = new byte[1];
        r.nextBytes(data);
        test(data);
        data = new byte[0];
        test(data);
        data = new byte[256];
        r.nextBytes(data);
        test(data);
    }

    static void test(String test) {
        test(test.getBytes());
    }

    static void test(byte[] test) {
        String code = Base64.encode(test);
        byte[] res = Base64.decode(code);
        String str = "data:" + new String(test) + "\ncode: " + code
                + "\nres :" + new String(res);
        if (!Arrays.equals(test, res)) {
            throw new IllegalStateException(str);
        } else {
            System.out.println(str);
        }
    }
}
