package test;

import com.primankaden.crypto.arc4.ARC4InputStream;
import com.primankaden.crypto.arc4.ARC4OutputStream;

import java.io.*;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by PrimankaDEN on 12.02.2017.
 */
public class ARC4Test {
    public static void main(String[] args) throws IOException {
        test("12345678", "test");
        test("q", "password1234567890");
        test("123456781234qwertadfasdf", "1");
        test("1", "1");
        test("", "1");

        Random r = new Random();
        byte[] data = new byte[10];
        byte[] key = new byte[10];
        r.nextBytes(data);
        r.nextBytes(key);
        test(data, key);

        data = new byte[4096];
        key = new byte[256];
        r.nextBytes(data);
        r.nextBytes(key);
        test(data, key);

        data = new byte[1];
        key = new byte[10];
        r.nextBytes(data);
        r.nextBytes(key);
        test(data, key);
    }


    private static void test(String data, String key) throws IOException {
        test(data.getBytes(), key.getBytes());
    }

    private static void test(byte[] data, byte[] key) throws IOException {
        int size = data.length;
        ByteArrayOutputStream testOutputStream = new ByteArrayOutputStream(size);
        OutputStream os = new ARC4OutputStream(testOutputStream, key);
        os.write(data);
        os.close();
        System.out.println("encoded: " + new String(testOutputStream.toByteArray()));

        ByteArrayInputStream testInputStream = new ByteArrayInputStream(testOutputStream.toByteArray());
        InputStream is = new ARC4InputStream(testInputStream, key);
        byte[] b = new byte[size];
        is.read(b);
        String print = "data:   " + new String(data) + "\nresult: " + new String(b) + "\n";
        if (!Arrays.equals(b, data)) {
            throw new IllegalStateException(print);
        } else {
            System.out.println(print);
        }
    }
}
