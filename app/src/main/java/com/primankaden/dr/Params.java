package com.primankaden.dr;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.primankaden.crypto.Crypto;
import com.primankaden.dr.prf.Prf;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

public class Params implements Serializable {
    private Prf prf = Prf.getAvailableDigests().get(0);
    private int kdfCount = Crypto.DEFAULT_KDF_COUNT;
    private int kdfKeyLength = Crypto.DEFAULT_KDF_KEY_LENGTH;
    private int ivLength = Crypto.DEFAULT_IV_LENGTH;

    public Params() {
    }

    public Params(@NonNull String mask) throws IllegalArgumentException, NoSuchAlgorithmException {
        String[] s = mask.split(":");
        if (s.length != 3)
            throw new IllegalArgumentException();
        prf = Prf.byTag(s[0]);
        if (prf == null) throw new NoSuchAlgorithmException();
        try {
            kdfCount = Integer.parseInt(s[1]);
            kdfKeyLength = Integer.parseInt(s[2]);
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    public String mask() {
        return String.format(Locale.getDefault(), "%s:%d:%d", prf.getTag(), kdfCount, kdfKeyLength);
    }

    @NonNull
    public Prf getPrf() {
        return prf;
    }

    public void setPrf(@NonNull Prf prf) {
        this.prf = prf;
    }

    public int getKdfCount() {
        return kdfCount;
    }

    public void setKdfCount(int kdfCount) {
        this.kdfCount = kdfCount;
    }

    public int getKdfKeyLength() {
        return kdfKeyLength;
    }

    public void setKdfKeyLength(int kdfKeyLength) {
        this.kdfKeyLength = kdfKeyLength;
    }

    public int getIvLength() {
        return ivLength;
    }

    public void setIvLength(int ivLength) {
        this.ivLength = ivLength;
    }

    public static class Saver {
        private final static String SP_NAME = "paramsSp",
                KDF_COUNT_TAG = "kdfCount",
                PRF_TAG = "prf",
                KDF_KEY_LENGTH_TAG = "kdfLength",
                IV_LENGTH = "ivLength";

        public static void save(Context context, Params params) {
            SharedPreferences sp = context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE);
            sp.edit().putInt(KDF_COUNT_TAG, params.getKdfCount())
                    .putInt(KDF_KEY_LENGTH_TAG, params.getKdfKeyLength())
                    .putString(PRF_TAG, params.getPrf().getTag())
                    .putInt(IV_LENGTH, params.getIvLength())
                    .apply();
        }

        public static Params get(Context context) {
            Params params = new Params();
            SharedPreferences sp = context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE);
            Prf prf = Prf.byTag(sp.getString(PRF_TAG, ""));
            params.setPrf(prf == null ? Prf.getAvailableDigests().get(0) : prf);
            params.setKdfCount(sp.getInt(KDF_COUNT_TAG, Crypto.DEFAULT_KDF_COUNT));
            params.setKdfKeyLength(sp.getInt(KDF_KEY_LENGTH_TAG, Crypto.DEFAULT_KDF_KEY_LENGTH));
            params.setIvLength(sp.getInt(IV_LENGTH, Crypto.DEFAULT_IV_LENGTH));
            return params;
        }
    }

    @Override
    public String toString() {
        return prf.getTag() + ":" + kdfCount + ":" + kdfKeyLength + ":" + ivLength;
    }

    public static Params fromString(String code) {
        Params p = new Params();
        String[] codes = code.split(":");
        p.prf = Prf.byTag(codes[0]);
        p.kdfCount = Integer.parseInt(codes[1]);
        p.kdfKeyLength = Integer.parseInt(codes[2]);
        p.ivLength = Integer.parseInt(codes[3]);
        return p;
    }
}
