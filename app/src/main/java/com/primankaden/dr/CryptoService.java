package com.primankaden.dr;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.primankaden.crypto.Crypto;

public final class CryptoService extends Service {
    private final static int SERVICE_ID = 1001;

    private final static String PASS_ACTION = "passAction",
            PARAMS_ACTION = "paramsAction",
            PASTE_ACTION = "pasteAction",
            ENCRYPT_ACTION = "encryptAction",
            DECRYPT_ACTION = "decryptAction",
            START_ACTION = "startAction",
            STOP_ACTION = "stopAction";
    private final static String PASS_EXTRA = "pass",
            TEXT_EXTRA = "text",
            PARAMS_EXTRA = "extra";
    private final ClipboardManager clipboard;

    private String pass;
    private Crypto crypto;
    private String text;
    private NotificationCompat.Builder builder;
    private RemoteViews remoteViews;

    private volatile Looper mServiceLooper;
    private volatile ServiceHandler mServiceHandler;

    public CryptoService() {
        clipboard = (ClipboardManager) App.getContext().getSystemService(Context.CLIPBOARD_SERVICE);
    }

    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            onHandleIntent((Intent) msg.obj);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        HandlerThread thread = new HandlerThread(CryptoService.class.getSimpleName());
        thread.start();

        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }

    @Override
    public void onDestroy() {
        mServiceLooper.quit();
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new Binder();
    }

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, CryptoService.class);
        intent.setAction(START_ACTION);
        return intent;
    }

    public static void stop(Context context) {
        Intent intent = new Intent(context, CryptoService.class);
        intent.setAction(STOP_ACTION);
        context.startService(intent);
    }

    public static void setText(Context context, String text) {
        Intent intent = new Intent(context, CryptoService.class);
        intent.setAction(PASTE_ACTION);
        intent.putExtra(TEXT_EXTRA, text);
        context.startService(intent);
    }

    public static void setPass(Context context, String masterPass) {
        Intent intent = new Intent(context, CryptoService.class);
        intent.setAction(PASS_ACTION);
        intent.putExtra(PASS_EXTRA, masterPass);
        context.startService(intent);
    }

    public static void setParams(Context context, Params params) {
        Intent intent = new Intent(context, CryptoService.class);
        intent.setAction(PARAMS_ACTION);
        intent.putExtra(PARAMS_EXTRA, params.toString());
        context.startService(intent);
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        msg.obj = intent;
        mServiceHandler.sendMessage(msg);
        return START_STICKY;
    }

    protected void onHandleIntent(Intent intent) {
        switch (intent.getAction()) {
            case PARAMS_ACTION:
                Params params = Params.fromString(intent.getStringExtra(PARAMS_EXTRA));
                crypto = new Crypto(params.getPrf().getPrf(), params.getKdfCount(), params.getKdfKeyLength(), params.getIvLength());
                break;
            case PASS_ACTION:
                pass = intent.getStringExtra(PASS_EXTRA);
                break;
            case PASTE_ACTION:
                if (intent.hasExtra(TEXT_EXTRA)) {
                    text = intent.getStringExtra(TEXT_EXTRA);
                } else {
                    text = clipboard.getText().toString();
                }
                notifyWidget(text, "");
                break;
            case DECRYPT_ACTION:
                decrypt();
                break;
            case ENCRYPT_ACTION:
                encrypt();
                break;
            case START_ACTION:
                showWidget();
                break;
            case STOP_ACTION:
                stopForeground(true);
                stopSelf();
                break;
            default:
                throw new IllegalStateException();
        }
    }

    private String decrypt() {
        if (!Crypto.isCorrect(text)) {
            Toast.makeText(this, R.string.wrong_format, Toast.LENGTH_SHORT).show();
            notifyWidget(text, getString(R.string.wrong_format));
            return "";
        } else {
            String result = crypto.decrypt(pass, text);
            ClipData clip = ClipData.newPlainText(result, result);
            clipboard.setPrimaryClip(clip);
            Toast.makeText(this, "Copied to clipboard", Toast.LENGTH_SHORT).show();
            notifyWidget(text, result);
            return result;
        }
    }

    private String encrypt() {
        String result = crypto.encrypt(pass, text);
        ClipData clip = ClipData.newPlainText(result, result);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(this, "Copied to clipboard", Toast.LENGTH_SHORT).show();
        notifyWidget(text, result);
        return result;
    }

    public void notifyWidget(String text, String result) {
        remoteViews.setTextViewText(R.id.plaintext, text);
        remoteViews.setTextViewText(R.id.ciphertext, result);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = builder.build();
        notification.flags = Notification.FLAG_ONGOING_EVENT | Notification.FLAG_NO_CLEAR;
        manager.notify(SERVICE_ID, notification);
    }

    public void showWidget() {
        Context context = this;
        remoteViews = new RemoteViews(context.getPackageName(), R.layout.notification);

        Intent intent = new Intent(context, CryptoService.class);
        intent.setAction(PASTE_ACTION);
        PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent, 0);
        remoteViews.setOnClickPendingIntent(R.id.paste, pendingIntent);

        intent = new Intent(context, CryptoService.class);
        intent.setAction(DECRYPT_ACTION);
        pendingIntent = PendingIntent.getService(context, 0, intent, 0);
        remoteViews.setOnClickPendingIntent(R.id.decrypt, pendingIntent);

        intent = new Intent(context, CryptoService.class);
        intent.setAction(ENCRYPT_ACTION);
        pendingIntent = PendingIntent.getService(context, 0, intent, 0);
        remoteViews.setOnClickPendingIntent(R.id.encrypt, pendingIntent);

        intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        remoteViews.setTextViewText(R.id.plaintext, text);
        builder = new NotificationCompat.Builder(context)
                .setCustomContentView(remoteViews)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent);

        Notification notification = builder.build();
        notification.flags = Notification.FLAG_ONGOING_EVENT | Notification.FLAG_NO_CLEAR;
        startForeground(SERVICE_ID, notification);
    }

    public class Binder extends android.os.Binder {
        public String encrypt() {
            return CryptoService.this.encrypt();
        }

        public String decrypt() {
            return CryptoService.this.decrypt();
        }
    }
}
