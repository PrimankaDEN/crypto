package com.primankaden.dr;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.primankaden.crypto.Crypto;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class MainActivity extends AppCompatActivity {
    private static final int SETTINGS_REQUEST_CODE = 1001;
    @BindView(R.id.password)
    protected EditText passwordView;
    @BindView(R.id.plaintext)
    protected EditText plainTextView;
    @BindView(R.id.encrypt)
    protected Button encryptBtn;
    @BindView(R.id.decrypt)
    protected Button decryptBtn;
    @BindView(R.id.clear)
    protected View clearBtn;
    @BindView(R.id.result)
    protected TextView resultView;
    @BindView(R.id.copy)
    protected ImageButton copyBtn;
    @BindView(R.id.incorrect_format_error)
    protected View incorrectFormatErrorView;
    @BindView(R.id.incorrect_output_error)
    protected View incorrectOutputErrorView;

    private ClipboardManager clipboard;
    private CryptoService.Binder binder;
    private Intent serviceIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        updateParams();
        clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        serviceIntent = CryptoService.getStartIntent(this);
        startService(serviceIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        bindService(serviceIntent, serviceConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unbindService(serviceConnection);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CryptoService.stop(this);
    }

    private void updateParams() {
        Params params = Params.Saver.get(this);
        CryptoService.setParams(this, params);
        copyBtn.setEnabled(false);
    }

    @OnTextChanged(R.id.password)
    protected void onPassChanged(CharSequence pass) {
        CryptoService.setPass(this, pass.toString());
        copyBtn.setEnabled(false);
        incorrectFormatErrorView.setVisibility(View.GONE);
    }

    @OnTextChanged(R.id.plaintext)
    protected void onTextChanged(CharSequence text) {
        CryptoService.setText(this, text.toString());
        copyBtn.setEnabled(false);
        incorrectFormatErrorView.setVisibility(View.GONE);
    }

    @OnClick(R.id.clear)
    protected void onClearClick() {
        plainTextView.setText("");
    }

    @OnClick(R.id.encrypt)
    protected void onEncryptClick() {
        if (binder == null) {
            bindService(serviceIntent, serviceConnection, BIND_AUTO_CREATE);
        }
        String result = binder.encrypt();
        resultView.setText(result);
        copyBtn.setEnabled(!TextUtils.isEmpty(result));
    }

    @OnClick(R.id.decrypt)
    protected void onDecryptView() {
        if (binder == null) {
            bindService(serviceIntent, serviceConnection, BIND_AUTO_CREATE);
        }
        String text = plainTextView.getText().toString();
        if (Crypto.isCorrect(text)) {
            String result = binder.decrypt();
            resultView.setText(result);
            copyBtn.setEnabled(!TextUtils.isEmpty(result));
        } else {
            incorrectFormatErrorView.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.copy)
    protected void onCopyClick() {
        String text = resultView.getText().toString();
        ClipData clip = ClipData.newPlainText(text, text);
        clipboard.setPrimaryClip(clip);
    }

    @OnClick(R.id.paste)
    protected void onPasteClick() {
        String text = String.valueOf(clipboard.getText());
        plainTextView.setText(text);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.settings) {
            startActivityForResult(new Intent(this, SettingsActivity.class), SETTINGS_REQUEST_CODE);
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SETTINGS_REQUEST_CODE && resultCode == RESULT_OK) {
            updateParams();
        } else
            super.onActivityResult(requestCode, resultCode, data);
    }

    private final ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            binder = (CryptoService.Binder) service;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            binder = null;
        }
    };
}
