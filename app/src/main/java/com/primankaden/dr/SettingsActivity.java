package com.primankaden.dr;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.primankaden.dr.prf.Prf;

import java.security.NoSuchAlgorithmException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class SettingsActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    @BindView(R.id.iv_length)
    protected EditText ivLengthView;
    @BindView(R.id.hash)
    protected Spinner hashSpinner;
    @BindView(R.id.kdf_count)
    protected EditText kdfCountView;
    @BindView(R.id.kdf_key_length)
    protected EditText kdfKeyLengthView;
    @BindView(R.id.mask)
    protected TextView maskView;
    @BindView(R.id.error)
    protected View maskErrorView;
    @BindView(R.id.no_algorithm_error)
    protected View noAlgorithmError;
    @BindView(R.id.copy)
    protected Button copyView;
    @BindView(R.id.paste)
    protected Button pasteView;

    private ClipboardManager clipboard;
    private Params params;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);

        clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        params = Params.Saver.get(this);
        updateParams(params);
        hashSpinner.setAdapter(Prf.getDigestAdapter(this));
        hashSpinner.setOnItemSelectedListener(this);
    }

    private void updateParams(Params params) {
        ivLengthView.setText(String.valueOf(params.getIvLength()));
        kdfCountView.setText(String.valueOf(params.getKdfCount()));
        kdfKeyLengthView.setText(String.valueOf(params.getKdfKeyLength()));
        hashSpinner.setSelection(Prf.ordinal(params.getPrf()));
    }

    @OnTextChanged(R.id.kdf_count)
    protected void onKdfCountChanged(CharSequence text) {
        try {
            params.setKdfCount(Integer.parseInt(text.toString()));
        } catch (Exception ignored) {
        }
        updateMask();
    }

    @OnTextChanged(R.id.kdf_key_length)
    protected void onKdfKeyLengthChanged(CharSequence text) {
        try {
            params.setKdfKeyLength(Integer.parseInt(text.toString()));
        } catch (Exception ignored) {
        }
        updateMask();
    }

    @OnTextChanged(R.id.iv_length)
    protected void onIvLengthChanged(CharSequence text) {
        try {
            params.setIvLength(Integer.parseInt(text.toString()));
        } catch (Exception ignored) {
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.done) {
            Params.Saver.save(this, params);
            setResult(RESULT_OK);
            finish();
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }

    private void updateMask() {
        maskView.setText(params.mask());
        maskErrorView.setVisibility(View.GONE);
        noAlgorithmError.setVisibility(View.GONE);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        params.setPrf(Prf.getAvailableDigests().get(position));
        updateMask();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @OnClick(R.id.paste)
    protected void onPasteClick() {
        String mask = String.valueOf(clipboard.getText());
        try {
            params = new Params(mask);
            updateParams(params);
        } catch (IllegalArgumentException e) {
            maskView.setText(mask);
            maskErrorView.setVisibility(View.VISIBLE);
        } catch (NoSuchAlgorithmException e) {
            maskView.setText(mask);
            noAlgorithmError.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.copy)
    protected void onCopyClick() {
        String mask = maskView.getText().toString();
        ClipData clip = ClipData.newPlainText(mask, mask);
        clipboard.setPrimaryClip(clip);
    }
}
