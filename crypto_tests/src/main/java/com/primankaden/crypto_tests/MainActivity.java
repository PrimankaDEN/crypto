package com.primankaden.crypto_tests;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.primankaden.crypto.pbkdf2.PBKDF2;

import java.util.Date;
import java.util.Locale;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    @BindView(R.id.test_count)
    protected EditText testCountView;
    @BindView(R.id.key_length)
    protected EditText keyLengthView;
    @BindView(R.id.hash)
    protected Spinner hashSpinner;
    @BindView(R.id.count)
    protected EditText countView;
    @BindView(R.id.result)
    protected TextView resultView;
    @BindView(R.id.progress_bar)
    protected ProgressBar progressBar;

    private Prf prf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        hashSpinner.setAdapter(Prf.getDigestAdapter(this));
        hashSpinner.setOnItemSelectedListener(this);
    }

    private AsyncTask<Void, Void, Void> task;

    @OnClick(R.id.kdf_test)
    protected void onKdfTestClick() {
        if (task != null) {
            task.cancel(false);
        }
        task = new AsyncTask<Void, Void, Void>() {
            private Date from, to;
            private int testCount, count, keyLength;
            private PBKDF2.PRF prf;
            final Runtime runtime = Runtime.getRuntime();
            long mem, memAfter;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                testCount = Integer.parseInt(testCountView.getText().toString());
                count = Integer.parseInt(countView.getText().toString());
                keyLength = Integer.parseInt(keyLengthView.getText().toString());
                this.prf = MainActivity.this.prf.getPrf();
                progressBar.setVisibility(View.VISIBLE);
                progressBar.setMax(testCount);
                from = new Date();
            }

            @Override
            protected Void doInBackground(Void... params) {
                byte[] pass = new byte[20];
                byte[] iv = new byte[20];
                Random r = new Random();
                r.nextBytes(pass);
                r.nextBytes(iv);
                mem = (runtime.totalMemory() - runtime.freeMemory()) / 1024L;
                for (int i = 0; i < testCount; i++) {
                    if (isCancelled()) {
                        return null;
                    }
                    PBKDF2.pbkdf2(prf, pass, iv, count, keyLength);
                }
                memAfter = (runtime.totalMemory() - runtime.freeMemory()) / 1024L;
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                progressBar.setVisibility(View.GONE);
                to = new Date();
                long delta = to.getTime() - from.getTime();
                long average = delta / testCount;
                resultView.setVisibility(View.VISIBLE);
                resultView.setText(String.format(Locale.getDefault(),
                        "Prf test. Total %d, average %d\n Mem before %d, mem after %d, %d",
                        delta, average, mem, memAfter, memAfter - mem));
            }
        };
        task.execute();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        prf = Prf.getAvailableDigests().get(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
