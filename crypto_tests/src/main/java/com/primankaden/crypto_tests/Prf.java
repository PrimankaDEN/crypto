package com.primankaden.crypto_tests;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.ArrayAdapter;

import com.primankaden.crypto.pbkdf2.PBKDF2;
import com.primankaden.crypto.pbkdf2.prf.HmacDigestPRF;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.Security;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

public class Prf {
    private final String title;
    private PBKDF2.PRF prf;
    private final String tag;
    private final String algorithm;

    public Prf(String title, @NonNull PBKDF2.PRF prf, String tag) {
        this.title = title;
        this.prf = prf;
        this.tag = tag;
        this.algorithm = null;
    }

    public PBKDF2.PRF getPrf() {
        if (prf == null) {
            try {
                prf = new HmacDigestPRF(MessageDigest.getInstance(algorithm));
            } catch (NoSuchAlgorithmException ignored) {
            }
        }
        return prf;
    }

    public String getTag() {
        return tag;
    }

    @Override
    public String toString() {
        return title;
    }

    public static ArrayAdapter<Prf> getDigestAdapter(Context context) {
        return new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, getAvailableDigests());
    }

    private static List<Prf> availableDigests;

    public static List<Prf> getAvailableDigests() {
        if (availableDigests != null) {
            return availableDigests;
        } else {
            availableDigests = new ArrayList<>();
            Provider[] providers = Security.getProviders();
            List<String> addedAlgorithms = new ArrayList<>();
            for (Provider provider : providers) {
                Set<Provider.Service> services = provider.getServices();
                for (Provider.Service service : services) {
                    if (service.getType().equalsIgnoreCase("MessageDigest")) {
                        try {
                            MessageDigest md = MessageDigest.getInstance(service.getAlgorithm());
                            String algorithm = md.getAlgorithm();
                            if (!addedAlgorithms.contains(algorithm)) {
                                if (md.getDigestLength() > 0) {
                                    addedAlgorithms.add(algorithm);
                                    availableDigests.add(new Prf(algorithm, new HmacDigestPRF(md), algorithm));
                                }
                            }
                        } catch (NoSuchAlgorithmException e) {
                            e.printStackTrace();
                        }

                    }
                }
                Collections.sort(availableDigests, new Comparator<Prf>() {
                    @Override
                    public int compare(Prf o1, Prf o2) {
                        return o2.getPrf().getHLen() - o1.getPrf().getHLen();
                    }
                });
            }
            return Prf.availableDigests;
        }
    }

    @Nullable
    public static Prf byTag(String tag) {
        for (Prf prf : getAvailableDigests()) {
            if (prf.getTag().equals(tag)) {
                return prf;
            }
        }
        return null;
    }

    public static int ordinal(Prf prf) {
        List<Prf> availableDigests1 = getAvailableDigests();
        for (int i = 0; i < availableDigests1.size(); i++) {
            Prf p = availableDigests1.get(i);
            if (p.equals(prf)) {
                return i;
            }
        }
        return 0;
    }
}
