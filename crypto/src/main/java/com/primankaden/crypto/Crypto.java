package com.primankaden.crypto;

import android.text.TextUtils;
import android.util.Log;

import com.primankaden.crypto.arc4.ARC4InputStream;
import com.primankaden.crypto.arc4.ARC4OutputStream;
import com.primankaden.crypto.base64.Base64;
import com.primankaden.crypto.pbkdf2.PBKDF2;
import com.primankaden.crypto.pbkdf2.prf.RandomPRF;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;
import java.util.regex.Pattern;

public class Crypto {
    public static final int DEFAULT_KDF_COUNT = 4096,
            DEFAULT_KDF_KEY_LENGTH = 256,
            DEFAULT_IV_LENGTH = 8;
    private final PBKDF2.PRF prf;
    private final int kdfCount;
    private final int kdfKeyLength;
    private final int ivLength;

    public Crypto() {
        this(new RandomPRF(128), DEFAULT_KDF_COUNT, DEFAULT_KDF_KEY_LENGTH, DEFAULT_IV_LENGTH);
    }

    public Crypto(PBKDF2.PRF prf, int kdfCount, int kdfKeyLength, int ivLength) {
        this.prf = prf;
        this.kdfCount = kdfCount;
        this.kdfKeyLength = kdfKeyLength;
        this.ivLength = ivLength;
    }

    private final static Pattern PATTERN = Pattern.compile("^[A-Za-z0-9-_]+[=]{0,2}:[A-Za-z0-9-_]+[=]{0,2}$");

    public static boolean isCorrect(String text) {
        return PATTERN.matcher(text).matches();
    }

    /**
     * @param key master password
     * @return строка в формате initialValue:cipherText
     */
    public String encrypt(String key, String plaintText) {
        if (TextUtils.isEmpty(plaintText)) {
            return "";
        }
        byte[] salt = generateSalt(ivLength);
        byte[] oneTimePassword = PBKDF2.pbkdf2(prf, key == null ? new byte[0] : key.getBytes(), salt, kdfCount, kdfKeyLength);
        if (oneTimePassword.length != kdfKeyLength)
            throw new IllegalStateException("Illegal key length");
        Log.d("&&", "salt:" + Base64.encode(salt) + ", pass:" + Base64.encode(oneTimePassword));
        int size = plaintText.getBytes().length;
        ByteArrayOutputStream testOutputStream = new ByteArrayOutputStream(size);
        OutputStream os = new ARC4OutputStream(testOutputStream,
                oneTimePassword);
        try {
            os.write(plaintText.getBytes());
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return String.format("%s:%s",
                Base64.encode(salt),
                Base64.encode(testOutputStream.toByteArray()));
    }

    /**
     * @param key  master password
     * @param data строка в формате initialValue:cipherText
     * @return исходный текст
     */
    public String decrypt(String key, String data) {
        String[] split = data.split(":");
        if (split.length != 2) {
            return "";
        }
        byte[] iv = Base64.decode(split[0]);
        byte[] text = Base64.decode(split[1]);
        byte[] oneTimePassword = PBKDF2.pbkdf2(prf, key == null ? new byte[0] : key.getBytes(), iv, kdfCount, kdfKeyLength);

        ByteArrayInputStream testInputStream = new ByteArrayInputStream(text);
        InputStream is = new ARC4InputStream(testInputStream, oneTimePassword);
        byte[] b = new byte[text.length];
        try {
            is.read(b);
        } catch (Exception ignored) {
        }
        return new String(b);
    }

    private static byte[] generateSalt(int length) {
        byte[] r = new byte[length];
        new Random().nextBytes(r);
        return r;
    }
}
