package com.primankaden.crypto.pbkdf2;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * Created by PrimankaDEN on 13.03.2017.
 */
public class PBKDF2 {
    public static byte[] pbkdf2(PRF prf, byte[] masterPass, byte[] salt, int count, int dkLen) {
        int hLen = prf.getHLen();
        // количество блоков длины hLen в ключе (округление вверх)
        int l = (dkLen + hLen - 1) / hLen;
        // количество байт в последнем блоке
        int r = dkLen - (l - 1) * hLen;

        byte[][] t = new byte[l][hLen];

        for (int i = 0; i < l; i++) {
            t[i] = f(prf, masterPass, salt, count, i + 1);
        }

        byte[] result = new byte[dkLen];
        for (int i = 0; i < l; i++) {
            int blockSize = (i == l - 1) ? r : hLen;
            System.arraycopy(t[i], 0, result, i * hLen, blockSize);
        }
        return result;
    }

    static byte[] f(PRF prf, byte[] masterPass, byte[] salt, int count, int index) {
        byte[] newSalt = Arrays.copyOf(salt, salt.length + 4);
        System.arraycopy(ByteBuffer.allocate(4).putInt(index).array(), 0, newSalt, salt.length, 4);
        byte[] u = prf.prf(masterPass, newSalt);
        byte[] result = Arrays.copyOf(u, u.length);
        for (int i = 1; i < count; i++) {
            u = prf.prf(masterPass, u);
            for (int j = 0; j < result.length; j++) {
                result[j] ^= u[j];
            }
        }
        return result;
    }


    public static abstract class PRF implements Serializable{
        private final int hLen;

        /**
         * @param hLen длина блока в байтах
         */
        public PRF(int hLen) {
            if (hLen == 0)
                throw new IllegalArgumentException();
            this.hLen = hLen;
        }

        public abstract byte[] prf(byte[] pass, byte[] iv);

        /**
         * @return длина блока в байтах
         */
        public final int getHLen() {
            return hLen;
        }
    }
}
