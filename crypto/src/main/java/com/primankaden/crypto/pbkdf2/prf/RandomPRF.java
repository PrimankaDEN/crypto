package com.primankaden.crypto.pbkdf2.prf;

import com.primankaden.crypto.pbkdf2.PBKDF2;

import java.util.Random;

/**
 * Created by PrimankaDEN on 02.04.2017.
 */
public class RandomPRF extends PBKDF2.PRF {
    public RandomPRF(int hLen) {
        super(hLen);
    }

    @Override
    public byte[] prf(byte[] pass, byte[] iv) {
        Random r = new Random(iv(iv, pass));
        byte[] result = new byte[getHLen()];
        r.nextBytes(result);
        return result;
    }

    private static long iv(byte[] iv, byte[] pass) {
        long result = 0;
        for (int i = 0; i < iv.length; i++) {
            result ^= ((long) iv[i]) << (8 * (i % 8));
        }
        for (int i = 0; i < pass.length; i++) {
            result ^= ((long) pass[i]) << (8 * (i % 8));
        }
        return result;
    }
}
