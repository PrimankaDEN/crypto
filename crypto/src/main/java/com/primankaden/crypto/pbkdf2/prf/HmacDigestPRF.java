package com.primankaden.crypto.pbkdf2.prf;

import android.support.annotation.NonNull;

import java.security.MessageDigest;

public class HmacDigestPRF extends HmacPRF {
    private final MessageDigest digest;

    public HmacDigestPRF(@NonNull MessageDigest digest) {
        super(digest.getDigestLength());
        this.digest = digest;
    }

    @Override
    protected byte[] hash(byte[] data) {
        return digest.digest(data);
    }
}
