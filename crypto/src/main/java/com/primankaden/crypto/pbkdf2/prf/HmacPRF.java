package com.primankaden.crypto.pbkdf2.prf;

import com.primankaden.crypto.pbkdf2.PBKDF2;

import java.util.Arrays;
import java.util.Locale;

public abstract class HmacPRF extends PBKDF2.PRF {
    public HmacPRF(int blockSize) {
        super(blockSize);
    }

    @Override
    public byte[] prf(byte[] pass, byte[] iv) {
        byte[] newPass = pass.length > getHLen() ?
                hashAndCheck(pass) : pass;
        newPass = newPass.length < getHLen() ?
                Arrays.copyOf(newPass, getHLen()) : newPass;
        byte[] ipad = new byte[getHLen()];
        Arrays.fill(ipad, (byte) 0x36);
        byte[] opad = new byte[getHLen()];
        Arrays.fill(opad, (byte) 0x5c);
        byte[] ikeypad = xor(ipad, newPass);
        byte[] okeypad = xor(opad, newPass);
        return hashAndCheck(concat(okeypad, hashAndCheck(concat(ikeypad, iv))));
    }

    private byte[] hashAndCheck(byte[] data) {
        byte[] result = hash(data);
        if (result.length != getHLen())
            throw new IllegalStateException(String.format(Locale.getDefault(), "Illegal block length %d, required %d", result.length, getHLen()));
        return result;
    }

    /**
     * @return хеш длины {@link HmacPRF#getHLen()} байт
     */
    protected abstract byte[] hash(byte[] data);

    private static byte[] xor(byte[] a, byte[] b) {
        if (a.length != b.length)
            throw new IllegalArgumentException();
        byte[] c = new byte[a.length];
        for (int i = 0; i < c.length; i++) {
            c[i] = (byte) (a[i] ^ b[i]);
        }
        return c;
    }

    private static byte[] concat(byte[] a, byte[] b) {
        int aLen = a.length;
        int bLen = b.length;

        byte[] c = new byte[aLen + bLen];
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);

        return c;
    }
}
