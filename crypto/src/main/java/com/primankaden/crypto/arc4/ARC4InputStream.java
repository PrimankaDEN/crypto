package com.primankaden.crypto.arc4;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by PrimankaDEN on 12.02.2017.
 */
public class ARC4InputStream extends InputStream {
    private InputStream in;
    private ARC4 arc4;

    public ARC4InputStream(InputStream in, byte[] key) {
        this.in = in;
        arc4 = new ARC4(key);
    }

    public ARC4InputStream(InputStream in, byte[] key, int wordSize, int voidBytes) {
        this.in = in;
        arc4 = new ARC4(key, wordSize, voidBytes);
    }

    @Override
    public int read() throws IOException {
        int c = in.read();
        if (c == -1) {
            return c;
        } else {
            return c ^ arc4.next();
        }
    }
}
