package com.primankaden.crypto.arc4;


class ARC4 {
    private static final int DEFAULT_WORD_SIZE = 8,
            DEFAULT_VOID_BYTES = 1024;
    private final int blockSize;
    private final int[] S;
    private int x, y;

    public ARC4(byte[] key) {
        this(key, DEFAULT_WORD_SIZE);
    }

    public ARC4(byte[] key, int wordSize) {
        this(key, wordSize, DEFAULT_VOID_BYTES);
    }

    public ARC4(byte[] key, int wordSize, int voidBytes) {
        if (key.length == 0)
            throw new IllegalArgumentException("key should be non null");
        int keyLength = key.length;
        blockSize = (int) Math.pow(2, wordSize);
        S = new int[blockSize];

        for (int i = 0; i < blockSize; i++) {
            S[i] = i;
        }

        int j = 0;
        for (int i = 0; i < blockSize; i++) {
            j = floorMod(j + S[i] + key[i % keyLength], blockSize);
            int temp = S[i];
            S[i] = S[j];
            S[j] = temp;
        }

        for (int i = 0; i < voidBytes; i++) {
            next();
        }
    }

    public int next() {
        x = (x + 1) % blockSize;
        y = (y + S[x]) % blockSize;
        int temp = S[x];
        S[x] = S[y];
        S[y] = temp;
        return S[(S[x] + S[y]) % blockSize];
    }

    private static int floorMod(int x, int y) {
        return x - floorDiv(x, y) * y;
    }

    private static int floorDiv(int x, int y) {
        int r = x / y;
        // if the signs are different and modulo not zero, round down
        if ((x ^ y) < 0 && (r * y != x)) {
            r--;
        }
        return r;
    }
}
