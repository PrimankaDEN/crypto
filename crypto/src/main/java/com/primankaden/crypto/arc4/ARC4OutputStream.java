package com.primankaden.crypto.arc4;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by PrimankaDEN on 12.02.2017.
 */
public class ARC4OutputStream extends OutputStream {
    private OutputStream out;
    private ARC4 arc4;

    public ARC4OutputStream(OutputStream out, byte[] key) {
        this.out = out;
        arc4 = new ARC4(key);
    }

    @Override
    public void write(int b) throws IOException {
        out.write(b ^ arc4.next());
    }
}
