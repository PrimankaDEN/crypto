package com.primankaden.crypto.base64;


import java.util.Arrays;

/**
 * Created by PrimankaDEN on 12.02.2017.
 */
public class Base64 {
    private static final char[] S = {
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
            'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
            'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
            'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
            'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
            'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
            'w', 'x', 'y', 'z', '0', '1', '2', '3',
            '4', '5', '6', '7', '8', '9', '-', '_'};

    public static String encode(String data) {
        return data.isEmpty() ? "" : encode(data.getBytes());
    }

    public static String encode(byte[] data) {
        if (data == null || data.length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        int added = 0;
        for (int i = 0; i < data.length; i += 3) {
            int buffer = 0;
            buffer |= (data[i] & 0xff) << 16;
            if (i + 2 < data.length) {
                buffer |= (data[i + 1] & 0xff) << 8;
                buffer |= (data[i + 2] & 0xff);
                added = 0;
            } else if (i + 1 < data.length) {
                buffer |= (data[i + 1] & 0xff) << 8;
                added = 1;
            } else {
                added = 2;
            }

            sb.append(S[(buffer >>> 18) & 0x3f]);
            sb.append(S[(buffer >>> 12) & 0x3f]);
            sb.append(S[(buffer >>> 6) & 0x3f]);
            sb.append(S[buffer & 0x3f]);
        }
        if (added > 0) {
            sb.delete(sb.length() - added, sb.length());
        }
        for (int i = 0; i < added; i++) {
            sb.append("=");
        }

        return sb.toString();
    }

    public static byte[] decode(String base64) {
        if (base64.isEmpty()) {
            return new byte[0];
        }
        int added = 0;
        if (base64.charAt(base64.length() - 2) == '=') {
            base64 = base64.substring(0, base64.length() - 2);
            base64 = String.format("%sAA", base64);
            added = 2;
        } else if (base64.charAt(base64.length() - 1) == '=') {
            base64 = base64.substring(0, base64.length() - 1);
            base64 = String.format("%sA", base64);
            added = 1;
        }

        char[] byteData = base64.toCharArray();
        byte[] result = new byte[(byteData.length / 4) * 3];
        for (int i = 0; i < byteData.length; i += 4) {
            int buffer = 0;
            buffer |= codeByChar(byteData[i]) << 18;
            buffer |= codeByChar(byteData[i + 1]) << 12;
            buffer |= codeByChar(byteData[i + 2]) << 6;
            buffer |= codeByChar(byteData[i + 3]);

            result[(i / 4) * 3] = (byte) ((buffer >>> 16) & 255);
            result[(i / 4) * 3 + 1] = (byte) ((buffer >>> 8) & 255);
            result[(i / 4) * 3 + 2] = (byte) ((buffer) & 255);
        }
        return Arrays.copyOf(result, result.length - added);
    }

    public static String decodeString(String data) {
        return new String(decode(data));
    }

    private static int codeByChar(char c) {
        for (int i = 0; i < 64; i++) {
            if (c == S[i]) {
                return i;
            }
        }
        return 0;
    }
}
